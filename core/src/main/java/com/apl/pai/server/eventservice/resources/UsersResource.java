package com.apl.pai.server.eventservice.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.apl.pai.server.eventservice.persistence.UserDao;
import com.apl.pai.server.eventservice.representations.User;
import com.apl.pai.server.exceptions.BadInputException;
import com.apl.pai.server.util.PasswordUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.yammer.metrics.annotation.Timed;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UsersResource {
    private UserDao userDao;

    public UsersResource(UserDao dao) {
        userDao = dao;
    }

    @POST
    @Path("/login")
    @Timed
    public String getUser(String userJsonString) throws Exception {
    	
    	ObjectMapper mapper = new ObjectMapper();
    	JsonNode userJson = mapper.readTree(userJsonString);
    	
    	User user = userDao.findByUsername(userJson.get("username").textValue());
        if (user != null) {
        	if( PasswordUtil.check(userJson.get("password").textValue(), user.getPassword()) ) {
        		return user.getUsername();
        	} else {
                throw new BadInputException("Wrong Credentials.");
        	}
        } else {
            throw new BadInputException("Wrong Credentials.");
        }
    }

    @POST
    @Path("/register")
    @Timed
    public String register(User user) throws Exception {
    	
        if (Strings.isNullOrEmpty(user.getUsername())) {
            throw new BadInputException("Username is missing.");
        }
        if (Strings.isNullOrEmpty(user.getPassword())) {
            throw new BadInputException("Password is missing.");
        }
    	user.setPassword(PasswordUtil.getSaltedHash(user.getPassword()));
    	
    	userDao.insert(user);
    	return user.getUsername();
    }

    @POST
    @Path("/password/change")
    @Timed
    public String changePassword(String userJsonString) throws Exception {
    	
    	ObjectMapper mapper = new ObjectMapper();
    	JsonNode userJson = mapper.readTree(userJsonString);
    	String username = userJson.get("username").textValue();
    	String oldPassword = userJson.get("oldPassword").textValue();
    	String newPassword = userJson.get("newPassword").textValue();
    	
    	User user = userDao.findByUsername(username);
        if (user != null) {
        	if( PasswordUtil.check(oldPassword, user.getPassword()) ) {
        		user.setPassword(newPassword);
        		userDao.update(user);
        		return username;
        	} else {
        		throw new BadInputException("User not found!");
        	}
        } else {
        	throw new BadInputException("User not found!");
        }
    }

    @GET
    @Path("/list")
    @Timed
    public List<User> listUsers() {
        return userDao.getAll();
    }

    @DELETE
    @Path("/delete/{username}")
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteUser(@PathParam("username") String username) {
        
        if (userDao.findByUsername(username) != null) {
            userDao.deleteByUsername(username);
        } else {
        	throw new BadInputException("User not found!");
        }
    }
}
