import React, {Component} from 'react';
import './Footer.less';
class Footer extends Component {

  render() {

    return (
      <footer className={'footer-main'}>
        <div className="container">
          <div className="text-center">© 2015 Jyotirmay Banerjee.</div>
        </div>
      </footer>
    );
  }
}

export default Footer;
