package com.apl.pai.server.exceptions;

public class AppException extends RuntimeException {
    private static final long serialVersionUID = 7352695164940434311L;

    public AppException(String why) {
        super(why);
    }

    public AppException(Exception e) {
        super(e);
    }

    public AppException(String why, Throwable e) {
        super(why, e);
    }
}
