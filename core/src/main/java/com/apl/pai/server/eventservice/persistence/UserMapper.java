package com.apl.pai.server.eventservice.persistence;

import com.apl.pai.server.eventservice.representations.User;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements ResultSetMapper<User> {
    @Override
    public User map(int i, ResultSet rs, StatementContext statementContext) throws SQLException {
        return new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"));
    }
}
