package com.apl.pai.server.eventservice.resources;

import com.yammer.metrics.annotation.Timed;
import com.apl.pai.server.eventservice.persistence.PersonDao;
import com.apl.pai.server.eventservice.representations.Person;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/persons")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PersonsResource {
    private PersonDao personDao;

    public PersonsResource(PersonDao dao) {
        personDao = dao;
    }

    @GET
    @Path("/{id}")
    @Timed
    public Person getPerson(@PathParam("id") Integer id) {
        Person p = personDao.findById(id);
        if (p != null) {
            return p;
        } else {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @GET
    @Timed
    public List<Person> listPersons() {
        return personDao.getAll();
    }

    @POST
    @Timed
    public List<Person> save(Person person) {
        if (person != null && person.isValid()) {
            personDao.insert(person);
            return personDao.getAll();
        } else {
        	throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @PUT
    @Timed
    public List<Person> update(Person person) {
        if (person != null && person.isValid()) {
            personDao.update(person);
            return personDao.getAll();
        } else {
        	throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @DELETE
    @Path("/{id}")
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Person> deletePerson(@PathParam("id") Integer id) {
        
        if (personDao.findById(id) != null) {
            personDao.deleteById(id);
            return personDao.getAll();
        } else {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }
}
