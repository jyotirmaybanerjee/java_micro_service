package com.apl.pai.server.eventservice.persistence;

import com.apl.pai.server.eventservice.representations.Person;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonMapper implements ResultSetMapper<Person> {
    @Override
    public Person map(int i, ResultSet rs, StatementContext statementContext) throws SQLException {
        return new Person(rs.getInt("id"), rs.getString("name"), rs.getString("email"), rs.getString("phone"));
    }
}
