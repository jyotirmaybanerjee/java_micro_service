@REM ----------------------------------------------------------------------------
@REM  Copyright 2001-2006 The Apache Software Foundation.
@REM
@REM  Licensed under the Apache License, Version 2.0 (the "License");
@REM  you may not use this file except in compliance with the License.
@REM  You may obtain a copy of the License at
@REM
@REM       http://www.apache.org/licenses/LICENSE-2.0
@REM
@REM  Unless required by applicable law or agreed to in writing, software
@REM  distributed under the License is distributed on an "AS IS" BASIS,
@REM  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@REM  See the License for the specific language governing permissions and
@REM  limitations under the License.
@REM ----------------------------------------------------------------------------
@REM
@REM   Copyright (c) 2001-2006 The Apache Software Foundation.  All rights
@REM   reserved.

@echo off

set ERROR_CODE=0

:init
@REM Decide how to startup depending on the version of windows

@REM -- Win98ME
if NOT "%OS%"=="Windows_NT" goto Win9xArg

@REM set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" @setlocal

@REM -- 4NT shell
if "%eval[2+2]" == "4" goto 4NTArgs

@REM -- Regular WinNT shell
set CMD_LINE_ARGS=%*
goto WinNTGetScriptDir

@REM The 4NT Shell from jp software
:4NTArgs
set CMD_LINE_ARGS=%$
goto WinNTGetScriptDir

:Win9xArg
@REM Slurp the command line arguments.  This loop allows for an unlimited number
@REM of arguments (up to the command line limit, anyway).
set CMD_LINE_ARGS=
:Win9xApp
if %1a==a goto Win9xGetScriptDir
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift
goto Win9xApp

:Win9xGetScriptDir
set SAVEDIR=%CD%
%0\
cd %0\..\.. 
set BASEDIR=%CD%
cd %SAVEDIR%
set SAVE_DIR=
goto repoSetup

:WinNTGetScriptDir
set BASEDIR=%~dp0\..

:repoSetup


if "%JAVACMD%"=="" set JAVACMD=java

if "%REPO%"=="" set REPO=%BASEDIR%\repo

set CLASSPATH="%BASEDIR%"\etc;"%REPO%"\com\yammer\dropwizard\dropwizard-core\0.6.2\dropwizard-core-0.6.2.jar;"%REPO%"\com\sun\jersey\jersey-core\1.17.1\jersey-core-1.17.1.jar;"%REPO%"\com\sun\jersey\jersey-server\1.17.1\jersey-server-1.17.1.jar;"%REPO%"\asm\asm\3.1\asm-3.1.jar;"%REPO%"\com\sun\jersey\jersey-servlet\1.17.1\jersey-servlet-1.17.1.jar;"%REPO%"\com\yammer\metrics\metrics-core\2.2.0\metrics-core-2.2.0.jar;"%REPO%"\com\yammer\metrics\metrics-servlet\2.2.0\metrics-servlet-2.2.0.jar;"%REPO%"\com\yammer\metrics\metrics-jetty\2.2.0\metrics-jetty-2.2.0.jar;"%REPO%"\com\yammer\metrics\metrics-logback\2.2.0\metrics-logback-2.2.0.jar;"%REPO%"\com\yammer\metrics\metrics-jersey\2.2.0\metrics-jersey-2.2.0.jar;"%REPO%"\com\yammer\metrics\metrics-annotation\2.2.0\metrics-annotation-2.2.0.jar;"%REPO%"\com\fasterxml\jackson\core\jackson-databind\2.1.4\jackson-databind-2.1.4.jar;"%REPO%"\com\fasterxml\jackson\core\jackson-annotations\2.1.4\jackson-annotations-2.1.4.jar;"%REPO%"\com\fasterxml\jackson\core\jackson-core\2.1.4\jackson-core-2.1.4.jar;"%REPO%"\com\fasterxml\jackson\jaxrs\jackson-jaxrs-json-provider\2.1.4\jackson-jaxrs-json-provider-2.1.4.jar;"%REPO%"\com\fasterxml\jackson\module\jackson-module-jaxb-annotations\2.1.4\jackson-module-jaxb-annotations-2.1.4.jar;"%REPO%"\com\fasterxml\jackson\dataformat\jackson-dataformat-yaml\2.1.4\jackson-dataformat-yaml-2.1.4.jar;"%REPO%"\com\fasterxml\jackson\datatype\jackson-datatype-guava\2.1.2\jackson-datatype-guava-2.1.2.jar;"%REPO%"\net\sourceforge\argparse4j\argparse4j\0.4.0\argparse4j-0.4.0.jar;"%REPO%"\org\slf4j\slf4j-api\1.7.4\slf4j-api-1.7.4.jar;"%REPO%"\org\slf4j\jul-to-slf4j\1.7.4\jul-to-slf4j-1.7.4.jar;"%REPO%"\ch\qos\logback\logback-core\1.0.10\logback-core-1.0.10.jar;"%REPO%"\ch\qos\logback\logback-classic\1.0.10\logback-classic-1.0.10.jar;"%REPO%"\org\slf4j\log4j-over-slf4j\1.7.4\log4j-over-slf4j-1.7.4.jar;"%REPO%"\org\eclipse\jetty\jetty-server\8.1.10.v20130312\jetty-server-8.1.10.v20130312.jar;"%REPO%"\org\eclipse\jetty\orbit\javax.servlet\3.0.0.v201112011016\javax.servlet-3.0.0.v201112011016.jar;"%REPO%"\org\eclipse\jetty\jetty-servlet\8.1.10.v20130312\jetty-servlet-8.1.10.v20130312.jar;"%REPO%"\org\eclipse\jetty\jetty-security\8.1.10.v20130312\jetty-security-8.1.10.v20130312.jar;"%REPO%"\org\eclipse\jetty\jetty-http\8.1.10.v20130312\jetty-http-8.1.10.v20130312.jar;"%REPO%"\org\eclipse\jetty\jetty-io\8.1.10.v20130312\jetty-io-8.1.10.v20130312.jar;"%REPO%"\com\google\guava\guava\13.0.1\guava-13.0.1.jar;"%REPO%"\com\google\code\findbugs\jsr305\2.0.1\jsr305-2.0.1.jar;"%REPO%"\org\hibernate\hibernate-validator\4.3.1.Final\hibernate-validator-4.3.1.Final.jar;"%REPO%"\javax\validation\validation-api\1.0.0.GA\validation-api-1.0.0.GA.jar;"%REPO%"\org\jboss\logging\jboss-logging\3.1.0.CR2\jboss-logging-3.1.0.CR2.jar;"%REPO%"\joda-time\joda-time\2.2\joda-time-2.2.jar;"%REPO%"\com\fasterxml\jackson\datatype\jackson-datatype-joda\2.1.2\jackson-datatype-joda-2.1.2.jar;"%REPO%"\com\yammer\dropwizard\dropwizard-jdbi\0.6.2\dropwizard-jdbi-0.6.2.jar;"%REPO%"\org\jdbi\jdbi\2.41\jdbi-2.41.jar;"%REPO%"\com\yammer\metrics\metrics-jdbi\2.2.0\metrics-jdbi-2.2.0.jar;"%REPO%"\com\yammer\dropwizard\dropwizard-db\0.6.2\dropwizard-db-0.6.2.jar;"%REPO%"\org\apache\tomcat\tomcat-dbcp\7.0.37\tomcat-dbcp-7.0.37.jar;"%REPO%"\com\h2database\h2\1.3.173\h2-1.3.173.jar;"%REPO%"\commons-codec\commons-codec\1.9\commons-codec-1.9.jar;"%REPO%"\org\eclipse\jetty\jetty-webapp\8.1.10.v20130312\jetty-webapp-8.1.10.v20130312.jar;"%REPO%"\org\eclipse\jetty\jetty-xml\8.1.10.v20130312\jetty-xml-8.1.10.v20130312.jar;"%REPO%"\org\eclipse\jetty\jetty-servlets\8.1.10.v20130312\jetty-servlets-8.1.10.v20130312.jar;"%REPO%"\org\eclipse\jetty\jetty-continuation\8.1.10.v20130312\jetty-continuation-8.1.10.v20130312.jar;"%REPO%"\org\eclipse\jetty\jetty-client\8.1.10.v20130312\jetty-client-8.1.10.v20130312.jar;"%REPO%"\org\eclipse\jetty\jetty-util\8.1.10.v20130312\jetty-util-8.1.10.v20130312.jar;"%REPO%"\org\glassfish\web\javax.servlet.jsp\2.2.3\javax.servlet.jsp-2.2.3.jar;"%REPO%"\javax\servlet\jsp\javax.servlet.jsp-api\2.2.1\javax.servlet.jsp-api-2.2.1.jar;"%REPO%"\com\apl\core\0.0.1-SNAPSHOT\core-0.0.1-SNAPSHOT.jar
goto endInit

@REM Reaching here means variables are defined and arguments have been captured
:endInit

%JAVACMD% %JAVA_OPTS%  -classpath %CLASSPATH_PREFIX%;%CLASSPATH% -Dapp.name="webapp" -Dapp.repo="%REPO%" -Dapp.home="%BASEDIR%" -Dbasedir="%BASEDIR%" com.apl.pai.WebApp %CMD_LINE_ARGS%
if %ERRORLEVEL% NEQ 0 goto error
goto end

:error
if "%OS%"=="Windows_NT" @endlocal
set ERROR_CODE=%ERRORLEVEL%

:end
@REM set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" goto endNT

@REM For old DOS remove the set variables from ENV - we assume they were not set
@REM before we started - at least we don't leave any baggage around
set CMD_LINE_ARGS=
goto postExec

:endNT
@REM If error code is set to 1 then the endlocal was done already in :error.
if %ERROR_CODE% EQU 0 @endlocal


:postExec

if "%FORCE_EXIT_ON_ERROR%" == "on" (
  if %ERROR_CODE% NEQ 0 exit %ERROR_CODE%
)

exit /B %ERROR_CODE%
