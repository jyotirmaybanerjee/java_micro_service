# JETTY SETUP #

Clone- 

```
git clone git@bitbucket.org:jyotirmaybanerjee/java_micro_service.git
```

Install maven dependencies -

```
cd java_micro_service && mvn clean install
```

Start Jetty server-

```
cd core && sh ./start_server.sh
```

Then open http://localhost:8080

default credential : user1/password


# BUILDING THE UI #

Install npm modules-

```
cd core/src/authclient && npm Install
```

Build code- 

```
npm run build
```

Debug server (node) (http://localhost:3000)

```
npm run debug
```

Run node server in production mode-
```
npm start
```

Build UI code and copy over to the jetty webapp. Need to package the fat jar for jetty again.
```
npm run release
```

Create fat jar for jetty server

```
npm package
```

Restart Jetty server

```
cd core && sh ./start_server.sh
```