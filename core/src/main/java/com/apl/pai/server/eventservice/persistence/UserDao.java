package com.apl.pai.server.eventservice.persistence;

import com.apl.pai.server.eventservice.representations.User;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(UserMapper.class)
public interface UserDao {
    @SqlUpdate("create table USER (id int auto_increment primary key, username varchar(80), password varchar(256))")
    void createUserTable();

    @SqlUpdate("insert into USER (username, password) values (:u.username, :u.password)")
    void insert(@BindBean("u") User user);

    @SqlUpdate("update USER set username = :u.username, password = :u.password where username = :u.username")
    void update(@BindBean("u") User user);
    
    @SqlUpdate("update USER set password = :u.password, where username = :u.username")
    void changePassword(@BindBean("u") User user);

    @SqlQuery("select * from USER where username = :username")
    User findByUsername(@Bind("username") String username);

    @SqlQuery("select * from USER")
    List<User> getAll();

    @SqlUpdate("delete from USER where username = :it")
    void deleteByUsername(@Bind String username);
}
