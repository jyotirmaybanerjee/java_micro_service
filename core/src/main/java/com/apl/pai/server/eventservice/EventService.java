package com.apl.pai.server.eventservice;

import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.Environment;
import com.apl.pai.server.eventservice.health.TemplateHealthCheck;
import com.apl.pai.server.eventservice.persistence.PersonDao;
import com.apl.pai.server.eventservice.persistence.UserDao;
import com.apl.pai.server.eventservice.representations.Person;
import com.apl.pai.server.eventservice.representations.User;
import com.apl.pai.server.eventservice.resources.PersonsResource;
import com.apl.pai.server.eventservice.resources.UsersResource;
import com.apl.pai.server.util.PasswordUtil;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.h2.jdbcx.JdbcConnectionPool;
import org.skife.jdbi.v2.DBI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EventService extends Service<EventConfiguration> {

    private static List<Person> persons;
    private static List<User> users;

    static {
        persons = Collections.synchronizedList(new ArrayList<Person>());
        persons.add(new Person("Jyo", "jyo@banerjee.com", "12345678"));
        persons.add(new Person("Manami", "manami@penchi.com"));
        persons.add(new Person("Mouli", "mouli@bhunia.com"));
        persons.add(new Person("Bachchu", "bachchu@ban.com"));
        persons.add(new Person("Sanju", "sanju@fun.com"));
        
        users = Collections.synchronizedList(new ArrayList<User>());
        try {
			users.add(new User("user1", PasswordUtil.getSaltedHash("password")));
	        users.add(new User("admin1", PasswordUtil.getSaltedHash("password")));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public static void main(String[] args) throws Exception {
        new EventService().run(args);
        startWeb();
    }

    @Override
    public void initialize(Bootstrap<EventConfiguration> bootstrap) {
        bootstrap.setName("server");
    }

    @Override
    public void run(EventConfiguration conf, Environment env) throws ClassNotFoundException {

    	String template = conf.getTemplate();
        JdbcConnectionPool jdbcConnectionPool = JdbcConnectionPool.create("jdbc:h2:mem:test2", "username", "password");
        DBI jdbi = new DBI(jdbcConnectionPool);
        PersonDao personDao = jdbi.onDemand(PersonDao.class);
        personDao.createPersonTable();
        seedPerson(personDao);

        UserDao userDao = jdbi.onDemand(UserDao.class);
        userDao.createUserTable();
        seedUser(userDao);
        
        env.addResource(new UsersResource(userDao));
        env.addResource(new PersonsResource(personDao));
        env.addHealthCheck(new TemplateHealthCheck(template));
    }

    private void seedPerson(PersonDao personDao) {
        for (Person p : persons) {
            personDao.insert(p);
        }
    }

    private void seedUser(UserDao userDao) {
        for (User u : users) {
        	userDao.insert(u);
        }
    }
    
    public static void startWeb() throws Exception {
        String webappDirLocation = "src/main/webapp/";
        String webPort = System.getenv("PORT");
        if (webPort == null || webPort.isEmpty()) {
            webPort = "8080";
        }
        Server server = new Server(Integer.valueOf(webPort));

        WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/");
        webapp.setDescriptor(webappDirLocation + "/WEB-INF/web.xml");
        webapp.setResourceBase(webappDirLocation);

        server.setHandler(webapp);
        server.start();
        server.join();
    }
}
